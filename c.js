var canvas = document.getElementById('myCanvas');
var ctx=canvas.getContext("2d");

ctx.font = "15px Arial" ;
ctx.fillStyle = "black" ;

ctx.lineJoin = "round" ;
ctx.lineCap = "round" ;
ctx.lineWidth = 5 ;

window.addEventListener( "mousedown" , mousedown ) ;
window.addEventListener( "mousemove" , mousemove ) ;
window.addEventListener( "mouseup" , mouseup ) ;
window.addEventListener( "keypress" , keypress ) ;
canvas.addEventListener("mousedown", text_type );

canvas.addEventListener( "mouseup" , function(){
	var imgData = ctx.getImageData(0, 0, 1000, 800);
	history[ ++cur_history ] = imgData ;
	if( cur_history == sz_history ) sz_history ++ ;
	else sz_history = cur_history + 1 ;
} ) ;

var click = false ;

var mode = "pen" ;
canvas.style.cursor='url(icon/pen.png), auto';

var pre_mouse_x , pre_mouse_y , mouse_x , mouse_y ;
var center_x, center_y ;
var history = [] ;
var cur_history = 0 , sz_history = 1 ;
history[ 0 ] = ctx.getImageData(0, 0, 1000, 800);

var textfont = "Arial" , textsize = 15 ;

function keypress( e ){

	if( e.key == "Enter" && mode == "text" ){
		var getinput = document.getElementById( "input" ) ;

		var getvalue = getinput.value ;
		ctx.font = textsize + "px " + textfont ;
		ctx.fillText( getvalue , leftnopx , topnopx ) ;
		getinput.remove() ;

		history[ ++cur_history ] = ctx.getImageData(0, 0, 1000, 800) ;
		if( cur_history == sz_history ) sz_history ++ ;
		else sz_history = cur_history + 1 ;
		
		text() ;

	}

}

function upload_image(event){
	
	var input = document.getElementById("image_upload");
	var img = new Image();
	img.src = window.URL.createObjectURL(input.files[0]);
	img.addEventListener( "load" , function(){
		input.value = "" ;
		ctx.drawImage( img ,0 , 0);
	} )
	history[ ++cur_history ] = ctx.getImageData(0, 0, 1000, 800) ;
	if( cur_history == sz_history ) sz_history ++ ;
	else sz_history = cur_history + 1 ;

}

function text(event){

	mode = 'text' ;
	canvas.style.cursor='url(icon/text.png), auto';
	type_finish = false ;

}

var type_finish = true ;

var topnopx ;
var leftnopx ;

function text_type(event){

	if( !type_finish && mode == "text" ){
		type_finish = true ;
		var input = document.createElement("input") ;
		input.id = "input" ;
		input.setAttribute("type", "text") ;
		input.setAttribute("autofocus", "true") ;
		input.style.position = 'absolute' ;
		input.style.top = event.pageY + 'px' ;
		input.style.left = event.pageX + 'px' ;

		topnopx = event.pageY ;
		leftnopx = event.pageX ;

		document.body.insertBefore( input , document.getElementById( "myCanvas" ) ) ;
	}

}

function f_pen(event){
	mode = 'pen';
	canvas.style.cursor='url(icon/pen.png), auto';
}
function f_eraser(event){
	mode = 'eraser';
	canvas.style.cursor='url(icon/eraser.png), auto';
}
function f_triangle(event){
	mode = 'triangle';
	canvas.style.cursor='url(icon/triangle.png), auto';
}
function f_circle(event){
	mode = 'circle';
	canvas.style.cursor='url(icon/circle.png), auto';
}
function f_square(event){
	mode = 'square';
	canvas.style.cursor='url(icon/square.png), auto';
}
function undo() {

	if( cur_history > 0 ){

		cur_history -- ;
		ctx.putImageData( history[ cur_history ] , 0 , 0 ) ;

	}

}

function redo() {

	console.log( sz_history ) ;
	if( cur_history < sz_history - 1 ){

		cur_history ++ ;
		ctx.putImageData( history[ cur_history ] , 0 , 0 ) ;
		
	}

}

function reset(event){

	ctx.clearRect(0, 0, 1000, 800) ;
	history = [] ;
	cur_history = 0 , sz_history = 1 ;
	history[ 0 ] = ctx.getImageData(0, 0, 1000, 800);

}

function mousedown(event){
	if( event.button == 0 ){

		click = true ;
		mouse_x = event.pageX ;
		mouse_y = event.pageY ;
		center_x = mouse_x ;
		center_y = mouse_y ;

	}
	
}

function mouseup(event){

	if( event.button == 0 ){
		click = false ;
	}
	
}

function mousemove(event){

	if( click ){

		pre_mouse_x = mouse_x ;
		pre_mouse_y = mouse_y ;
		mouse_x = event.pageX ;
		mouse_y = event.pageY ;
		if( mode == "pen" ){
			ctx.beginPath();
			ctx.moveTo( pre_mouse_x , pre_mouse_y ) ;
			ctx.lineTo( mouse_x , mouse_y ) ;
			ctx.stroke() ;
		}
		else if( mode == "eraser" ){
			ctx.lineWidth = 15 ;
			ctx.globalCompositeOperation = "destination-out" ;
			ctx.beginPath();
			ctx.moveTo( pre_mouse_x , pre_mouse_y ) ;
			ctx.lineTo( mouse_x , mouse_y ) ;
			ctx.stroke() ;
			ctx.globalCompositeOperation = "source-over" ;
		}
		else if( mode == "triangle" ){
			ctx.putImageData( history[ cur_history ] , 0 , 0 ) ;
			var dx = center_x - mouse_x ;
			var dy = center_y - mouse_y ;
			ctx.beginPath();
			ctx.moveTo( center_x, center_y ) ;
			ctx.lineTo( center_x + dx , center_y - dy ) ;
			ctx.lineTo( center_x - dx , center_y - dy ) ;
			ctx.lineTo( center_x, center_y ) ;
			ctx.stroke() ;
		}
		else if( mode == "circle" ){

			ctx.putImageData( history[ cur_history ] , 0 , 0 ) ;
			var radius = Math.sqrt( ( center_x - mouse_x ) ** 2 + ( center_y - mouse_y ) ** 2 ) ;
			ctx.beginPath() ;
			ctx.arc( mouse_x , mouse_y , radius , 0 , 2 * Math.PI ) ;
			ctx.stroke();

		}
		else if( mode == "square" ){

			ctx.putImageData( history[ cur_history ] , 0 , 0 ) ;
			ctx.strokeRect( center_x , center_y , mouse_x - center_x , mouse_y - center_y );

		}

	}

}
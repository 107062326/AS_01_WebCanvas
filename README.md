# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Basic components, Advanced tools 所有功能皆已完成, 工具列位於下側.    

    Brush : pen 按鈕
    eraser : eraser 按鈕
    Color selector : 下方有個色盤( 預設是黑色 )
    Simple menu : Brush 的大小調整 在 網頁最下方的拉霸
    
    Text input : text 按鈕
    Font menu : 提供 Text Size, Font 各兩種調整
                15px , 30px  , Arial , Console 
                鼠標移至按鈕上方, 清單會自動展開供選擇
                
    Refresh button : Reset 按鈕, 一旦Reset 後不可 Undo.
    
    Different brush shapes : circle , square , triangle 按鈕
    
    upload image : 選擇檔案按鈕
    
    Download image : 工具列有個下載圖示

### Function description

    沒有 Bonus Function, 但是有做一些 CSS 美觀.

### Gitlab page link

https://107062326.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    做得有點倉促, 抱歉傷害助教們的眼睛了QQ
    不過功能都有完成, 只來的及做一點點CSS.

